package out

import "wallet/pkg/domain"

type ClientOutput struct {
	Id   string `json:"id"`
	Cpf  string `json:"cpf"`
	Name string `json:"name"`
}

func ToClientOutput(c domain.Client) ClientOutput {

	return ClientOutput{
		Id:   c.Id,
		Cpf:  c.Cpf,
		Name: c.Name,
	}
}
