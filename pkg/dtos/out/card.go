package out

import "wallet/pkg/domain"

type CardOutput struct {
	Id       string `json:"id"`
	NumCard  string `json:"num_card"`
	DataVenc string `json:"data_venc"`
	ClientID string `json:"client_id"`
}

func ToCardOutput(c domain.Card) CardOutput {
	return CardOutput{
		Id:       c.Id,
		NumCard:  c.NumCard,
		DataVenc: c.DataVenc,
		ClientID: c.ClientID,
	}
}

type AllCardsOutput struct {
	Data []CardOutput `json:"data"`
}
