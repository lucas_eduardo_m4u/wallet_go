package in

import "wallet/pkg/domain"

type ClientInput struct {
	Cpf  string `json:"cpf" validate:"required,min=11"`
	Name string `json:"name" validate:"required,min=3"`
}

func (c ClientInput) ToClientDomain() domain.Client {

	return domain.Client{
		Cpf:  c.Cpf,
		Name: c.Name,
	}
}
