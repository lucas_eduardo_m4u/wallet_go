package in

import "wallet/pkg/domain"

type CardInput struct {
	NumCard  string `json:"num_card" validate:"required,min=16"`
	DataVenc string `json:"data_venc" validate:"required,min=7"`
}

func (c CardInput) ToCardDomain() domain.Card {
	return domain.Card{
		NumCard:  c.NumCard,
		DataVenc: c.DataVenc,
	}
}
