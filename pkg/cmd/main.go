package main

import "wallet/pkg/server"

func main() {
	app := server.NewApp()
	app.RunMigrations()

	app.StartServer()
}
