CREATE TABLE IF NOT EXISTS public.card
(
  id                        VARCHAR PRIMARY KEY,
  num_card                  VARCHAR                   NOT NULL,
  data_venc                 VARCHAR                   NOT NULL,
  client_id                 VARCHAR                   NOT NULL REFERENCES public.client (id)
);