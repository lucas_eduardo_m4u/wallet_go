ALTER TABLE public.client
    ADD UNIQUE (cpf);

ALTER TABLE public.card
    ADD UNIQUE (num_card);