CREATE TABLE IF NOT EXISTS public.client
(
  id                  VARCHAR                   PRIMARY KEY,
  cpf                 VARCHAR                   NOT NULL,
  name                VARCHAR                   NOT NULL
);