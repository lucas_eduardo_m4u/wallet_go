package database

import (
	"database/sql"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

func NewDatabaseConnection() (*bun.DB, error) {
	dns := "postgres://postgres:postgres@localhost:5432/walletgodb?sslmode=disable"
	sqldb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dns)))

	return bun.NewDB(sqldb, pgdialect.New()), nil
}
