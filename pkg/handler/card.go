package handler

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"wallet/pkg/dtos/in"
	"wallet/pkg/dtos/out"
	"wallet/pkg/errors"
	"wallet/pkg/repositories"
	"wallet/pkg/services"

	"github.com/go-chi/chi/v5"
	"github.com/uptrace/bun"
)

type HandlerCard struct {
	serviceCard services.IServiceCard
}

func NewHandlerCard(dbConnect bun.IDB, repositoryClient repositories.IRepositoryClient) HandlerCard {
	r := repositories.NewRepositoryCard(dbConnect)
	s := services.NewServiceCard(&r, repositoryClient)
	return HandlerCard{serviceCard: s}
}

func (h HandlerCard) CreateCard(w http.ResponseWriter, r *http.Request) {
	logger := log.WithContext(r.Context())

	clientID := chi.URLParam(r, "client_id")

	var newCard in.CardInput
	err := ReadRequest(r, &newCard)
	if err != nil {
		logger.Error("error to read body request", log.WithError(err))
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}

	err = Validate(newCard)
	if err != nil {
		if err, ok := errors.IsErrors(err); ok {
			logger.Error("validate error", log.WithError(err))
			responseErr := errors.NewReponseError(err.Error(), err.Code(), err.Details)
			WriteResponse(w, http.StatusBadRequest, responseErr)
			return
		}
		logger.Error("error to validate payload request", log.WithError(err))
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}
	logger.Info("request validated with success")

	createdCard, err := h.serviceCard.CreateCard(r.Context(), clientID, newCard)

	if err != nil {
		if err, ok := errors.IsErrors(err); ok {
			logger.Error("error to create a new card", log.WithError(err))
			response := errors.NewReponseError(err.Error(), err.Code(), err.Details)
			WriteResponse(w, http.StatusBadRequest, response)
			return
		}
	}
	logger.Info("new card created")

	response := out.ToCardOutput(createdCard)
	WriteResponse(w, http.StatusCreated, response)
}

func (h HandlerCard) GetCards(w http.ResponseWriter, r *http.Request) {
	logger := log.WithContext(r.Context())

	clientId := chi.URLParam(r, "client_id")

	cardList, err := h.serviceCard.GetCards(r.Context(), clientId)

	if err != nil {
		if err, ok := errors.IsErrors(err); ok {
			logger.Error("not found relational client", log.WithError(err))
			responseError := errors.NewReponseError(err.Error(), err.Code(), err.Details)
			WriteResponse(w, http.StatusNotFound, responseError)
			return
		}
	}

	var response out.AllCardsOutput

	if len(cardList) == 0 {
		response.Data = make([]out.CardOutput, 0)
		WriteResponse(w, http.StatusOK, response)
		return
	}

	for _, v := range cardList {
		response.Data = append(response.Data, out.ToCardOutput(v))
	}
	logger.Info("successful card search")
	WriteResponse(w, http.StatusOK, response)
	return
}
