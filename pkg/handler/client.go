package handler

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"wallet/pkg/dtos/in"
	"wallet/pkg/dtos/out"
	"wallet/pkg/errors"
	"wallet/pkg/repositories"
	"wallet/pkg/services"

	"github.com/uptrace/bun"
)

type HandlerClient struct {
	serviceClient services.IServiceClient
}

func NewHandlerClient(dbConnect bun.IDB) (HandlerClient, repositories.IRepositoryClient) {
	r := repositories.NewRepositoryClient(dbConnect)
	s := services.NewServiceClient(&r)
	return HandlerClient{serviceClient: s}, &r
}

func (h HandlerClient) CreateClient(w http.ResponseWriter, r *http.Request) {
	logger := log.WithContext(r.Context())
	//TODO: Diferença entre * e &

	var newClient in.ClientInput

	err := ReadRequest(r, &newClient)
	if err != nil {
		logger.Error("error to read body request", log.WithError(err))
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}

	err = Validate(newClient)
	if err != nil {
		if err, ok := errors.IsErrors(err); ok {
			logger.Error("validate error", log.WithError(err))
			responseErr := errors.NewReponseError(err.Error(), err.Code(), err.Details)
			WriteResponse(w, http.StatusBadRequest, responseErr)
			return
		}
		logger.Error("error to validate payload request", log.WithError(err))
		WriteResponse(w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("request validated with success")

	createdClient, err := h.serviceClient.CreateClient(logger.Context, newClient)

	if err != nil {
		if err, ok := errors.IsErrors(err); ok {
			logger.Error("error to create a new client", log.WithError(err))
			response := errors.NewReponseError(err.Error(), err.Code(), err.Details)
			WriteResponse(w, http.StatusBadRequest, response)
			return
		}
	}
	logger.Info("new client created")

	response := out.ToClientOutput(createdClient)
	WriteResponse(w, http.StatusCreated, response)
}
