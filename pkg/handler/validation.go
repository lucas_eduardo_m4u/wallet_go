package handler

import (
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	enTranslator "github.com/go-playground/validator/v10/translations/en"
	"wallet/pkg/errors"
)

var validate *validator.Validate
var uTranslator *ut.UniversalTranslator
var translator ut.Translator

func init() {
	english := en.New()
	uTranslator = ut.New(english, english)
	translator, _ = uTranslator.GetTranslator(english.Locale())

	validate = validator.New()
	enTranslator.RegisterDefaultTranslations(validate, translator)
}

func Validate(validateStruct interface{}) error {
	err := validate.Struct(validateStruct)
	if err != nil {
		validateError := err.(validator.ValidationErrors)

		invalidPayloadErr := errors.NewError("invalid payload request", errors.InvalidPayload, err)
		for _, v := range validateError {
			det := errors.NewDetails(v.Field(), v.Translate(translator))
			invalidPayloadErr.Details = append(invalidPayloadErr.Details, det)
		}

		return invalidPayloadErr
	}
	return nil
}
