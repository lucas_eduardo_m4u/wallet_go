package repositories

import (
	"context"
	"github.com/uptrace/bun/driver/pgdriver"
	"wallet/pkg/domain"
	"wallet/pkg/errors"

	"github.com/uptrace/bun"
)

type IRepositoryCard interface {
	InsertCard(ctx context.Context, cardReceived domain.Card) (domain.Card, error)
	GetCards(ctx context.Context, clientId string) ([]domain.Card, error)
}

type DatabaseCard struct {
	DB bun.IDB
}

func NewRepositoryCard(dbConnect bun.IDB) DatabaseCard {
	return DatabaseCard{
		DB: dbConnect,
	}
}

func (db DatabaseCard) InsertCard(ctx context.Context, cardReceived domain.Card) (domain.Card, error) {
	_, err := db.DB.NewInsert().Model(&cardReceived).Exec(ctx)

	if err, ok := err.(pgdriver.Error); ok && err.IntegrityViolation() {
		return domain.Card{}, errors.NewError(
			err.Error(),
			errors.ConstraintErr,
			err,
			errors.NewDetails(
				err.Field('n'), //err field name
				err.Field('D'), //err detail
			),
		)
	}

	if err != nil {
		return domain.Card{}, err
	}

	return cardReceived, nil
}

func (db DatabaseCard) GetCards(ctx context.Context, clientId string) ([]domain.Card, error) {
	cardReveived := make([]domain.Card, 0)
	err := db.DB.NewSelect().Model(&cardReveived).Where("client_id = ?", clientId).Scan(ctx)

	if err != nil {
		return []domain.Card{}, err
	}

	return cardReveived, nil

}
