package repositories

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/uptrace/bun/driver/pgdriver"
	"wallet/pkg/domain"
	"wallet/pkg/errors"

	"github.com/uptrace/bun"
)

type IRepositoryClient interface {
	GetClient(ctx context.Context, id string) (domain.Client, error)
	InsertClient(ctx context.Context, client domain.Client) (domain.Client, error)
}

type DatabaseClient struct {
	DB bun.IDB
}

func NewRepositoryClient(dbConnect bun.IDB) DatabaseClient {
	return DatabaseClient{
		DB: dbConnect,
	}
}

func (db DatabaseClient) GetClient(ctx context.Context, id string) (domain.Client, error) {
	var client domain.Client
	err := db.DB.NewSelect().Model(&client).Where("id = ?", id).Scan(ctx)

	if err != nil {
		if err == sql.ErrNoRows {
			return domain.Client{}, errors.NewError(
				"client not found",
				errors.ClientNotFound,
				err,
				errors.NewDetails("client_id", fmt.Sprintf("client with id %s is not found", id)),
			)
		} else {
			return domain.Client{}, err
		}
	}

	return client, err
}

func (db DatabaseClient) InsertClient(ctx context.Context, client domain.Client) (domain.Client, error) {
	_, err := db.DB.NewInsert().Model(&client).Exec(ctx)

	if err, ok := err.(pgdriver.Error); ok && err.IntegrityViolation() {
		return domain.Client{}, errors.NewError(
			err.Error(),
			errors.ConstraintErr,
			err,
			errors.NewDetails(
				err.Field('n'), //err field name
				err.Field('D'), //err detail
			),
		)
	}

	if err != nil {
		return domain.Client{}, err
	}

	return client, nil

}
