package domain

import "github.com/uptrace/bun"

type Client struct {
	bun.BaseModel `bun:"client"`
	Id            string `bun:"id,pk"`
	Cpf           string `bun:"cpf"`
	Name          string `bun:"name"`
}
