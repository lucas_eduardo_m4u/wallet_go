package domain

import "github.com/uptrace/bun"

type Card struct {
	bun.BaseModel `bun:"card"`
	Id            string `bun:"id,pk"`
	NumCard       string `bun:"num_card"`
	DataVenc      string `bun:"data_venc"`
	ClientID      string `bun:"client_id"`
}
