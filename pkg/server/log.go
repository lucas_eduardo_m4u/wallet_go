package server

import (
	logger "github.com/sirupsen/logrus"
	"os"
)

func configCtxLogger() {
	logger.SetOutput(os.Stdout)

	logger.SetFormatter(&logger.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FieldMap: logger.FieldMap{
			logger.FieldKeyMsg: "message",
		},
	})
}
