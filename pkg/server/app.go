package server

import (
	"context"
	"github.com/go-chi/chi/v5"
	logger "github.com/sirupsen/logrus"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/migrate"
	"net/http"
	"wallet/pkg/database"
	"wallet/pkg/database/migrations"
	"wallet/pkg/handler"
)

type App struct {
	router *chi.Mux
	dbConn *bun.DB
}

func NewApp() *App {
	var app App

	configCtxLogger()

	dbConnect, err := database.NewDatabaseConnection()
	if err != nil {
		logger.Panic("error to instantiate db connect")
		panic(err)
	}
	app.dbConn = dbConnect
	logger.Debug("db connect instantiated")

	handlerClient, repositoryClient := handler.NewHandlerClient(dbConnect)
	handlerCard := handler.NewHandlerCard(dbConnect, repositoryClient)
	logger.Debug("handler, service and repository instantiated")

	router := chi.NewRouter()

	router.Post("/clients", handlerClient.CreateClient)
	router.Post("/clients/{client_id}/cards", handlerCard.CreateCard)
	router.Get("/clients/{client_id}/cards", handlerCard.GetCards)
	logger.Debug("defined routes")

	app.router = router

	return &app
}

func (a *App) RunMigrations() {
	ctx := context.Background()
	migrator := migrate.NewMigrator(a.dbConn, migrations.Migrations)

	err := migrator.Init(ctx)
	if err != nil {
		logger.Fatal("error to init migrator")
		return
	}

	_, err = migrator.Migrate(ctx)
	if err != nil {
		logger.Fatal("error to Migrate")
		return
	}

	logger.Info("Migrations is up")
}

func (a *App) StartServer() {
	// TODO: veja como trabalhar com graceful shutdown
	err := http.ListenAndServe(":8080", a.router)
	if err != nil {
		logger.Fatal(err)
	}
}
