package services

import (
	"context"
	"github.com/google/uuid"
	"wallet/pkg/domain"
	"wallet/pkg/dtos/in"
	"wallet/pkg/repositories"
)

type IServiceCard interface {
	CreateCard(ctx context.Context, id string, card in.CardInput) (domain.Card, error)
	GetCards(ctx context.Context, client_id string) ([]domain.Card, error)
}

func NewServiceCard(rCard repositories.IRepositoryCard, rClient repositories.IRepositoryClient) ServiceCard {
	return ServiceCard{
		repositoryCard:   rCard,
		repositoryClient: rClient,
	}
}

type ServiceCard struct {
	repositoryCard   repositories.IRepositoryCard
	repositoryClient repositories.IRepositoryClient
}

func (s ServiceCard) CreateCard(ctx context.Context, clientId string, card in.CardInput) (domain.Card, error) {
	cardModel := in.CardInput.ToCardDomain(card)
	cardID := uuid.NewString()
	cardModel.Id = cardID
	cardModel.ClientID = clientId
	return s.repositoryCard.InsertCard(ctx, cardModel)
}

func (s ServiceCard) GetCards(ctx context.Context, clientId string) ([]domain.Card, error) {
	_, err := s.repositoryClient.GetClient(ctx, clientId)

	if err != nil {
		return []domain.Card{}, err
	}

	return s.repositoryCard.GetCards(ctx, clientId)
}
