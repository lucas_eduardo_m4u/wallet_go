package services

import (
	"context"
	"wallet/pkg/domain"
	"wallet/pkg/dtos/in"
	"wallet/pkg/repositories"

	"github.com/google/uuid"
)

func (s ServiceClient) CreateClient(ctx context.Context, client in.ClientInput) (domain.Client, error) {
	clientModel := in.ClientInput.ToClientDomain(client)
	clientId := uuid.NewString()
	clientModel.Id = clientId

	return s.repositoryClient.InsertClient(ctx, clientModel)
}

type IServiceClient interface {
	CreateClient(ctx context.Context, client in.ClientInput) (domain.Client, error)
}

func NewServiceClient(repository repositories.IRepositoryClient) ServiceClient {
	return ServiceClient{
		repositoryClient: repository,
	}
}

type ServiceClient struct {
	repositoryClient repositories.IRepositoryClient
}
