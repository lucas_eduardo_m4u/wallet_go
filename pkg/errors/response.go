package errors

type responseError struct {
	Message string    `json:"message"`
	Code    string    `json:"code"`
	Details []Details `json:"details"`
}

func NewReponseError(m string, c string, det []Details) responseError {
	return responseError{
		Message: m,
		Code:    c,
		Details: det,
	}
}
