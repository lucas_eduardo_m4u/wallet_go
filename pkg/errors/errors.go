package errors

const (
	ClientNotFound = "CLIENT_NOT_FOUND"
	InvalidPayload = "INVALID_PAYLOAD"
	ConstraintErr  = "CONSTRAINT_INTEGRITY_VIOLATION"
)

type Error struct {
	message string
	code    string
	err     error
	Details []Details
}

func NewError(m, c string, err error, det ...Details) Error {
	return Error{
		message: m,
		code:    c,
		err:     err,
		Details: det,
	}
}

type Details struct {
	Name   string
	Reason string
}

func NewDetails(n, r string) Details {
	return Details{
		Name:   n,
		Reason: r,
	}
}

func (e Error) Error() string {
	return e.message
}

func (e Error) Code() string {
	return e.code
}

func IsErrors(e error) (Error, bool) {
	err, ok := e.(Error)
	return err, ok
}
