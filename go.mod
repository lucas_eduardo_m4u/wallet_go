module wallet

go 1.17

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/uuid v1.3.0
	github.com/uptrace/bun v1.0.19

)

require (
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/uptrace/bun/driver/pgdriver v1.0.16
	// indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
)

require (
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/uptrace/bun/dialect/pgdialect v1.0.19
)

require (
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/text v0.3.6 // indirect
)

require (
	github.com/go-playground/validator/v10 v10.9.0
	github.com/jinzhu/inflection v1.0.0 // indirect
	mellium.im/sasl v0.2.1 // indirect
)
